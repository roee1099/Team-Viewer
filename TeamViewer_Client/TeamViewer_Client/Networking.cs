﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using ScreenShotDemo;
using TeamViewer_Server;
using Afkvalley;
using System.Runtime.InteropServices;
//using Renci.SshNet;

namespace TeamViewer_Client
{
    
    static class Networking
    {
        //private static SshClient sshclient = new SshClient("127.0.0.1","blah","123");
        private static Object sendLock = new Object();
        public static readonly TcpClient client = new TcpClient();
        public static  UdpClient udpClient = new UdpClient();
        private static NetworkStream mainStream;
        private static IPEndPoint endPoint;

        private const int UDP_BUFFER_SIZE = 60000;

        struct imageData
            {
            public int sliceNumber;
            public byte[] data;
            public bool last;
        };

        struct imageBaseData
        {
            public int sliceNumber;
            
            public bool last;
        };

        //private NetworkStream mainstream;
        public static void Connection(int port,string ip)
        {
            
            //testFunc();
            try
            {
                //ScreenCapture sc = new ScreenCapture();
                //Image img = sc.CaptureScreen();

                // new testDisplayForm(new Bitmap((Bitmap)img)).Show();

                


                client.Connect(ip, port);
                PartnerScreen.logWriteLine("Connected");

                //ip, port+1
                udpClient = new UdpClient();
                //udpClient.Connect(ip, port + 1);
                udpClient.Send(new byte[] { 2 }, 1, ip, port + 1);
                udpClient.Receive(ref endPoint);
                udpClient.Send(new byte[] { 2 }, 1, endPoint);
                udpClient.Connect(endPoint);

                Thread connection = new Thread(HandleConnection);
                connection.Start();
            }
            catch (Exception)
            {
                Console.WriteLine("failed to connect");
            }
        }

        private static byte[] getBytes(imageData iData)
        {
            imageBaseData str = new imageBaseData();
            str.sliceNumber = iData.sliceNumber;
            str.last = iData.last;

            int size = Marshal.SizeOf(str)+ iData.data.Length;
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(str));
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, Marshal.SizeOf(str));
            
            Marshal.FreeHGlobal(ptr);

            Buffer.BlockCopy(iData.data, 0, arr, Marshal.SizeOf(str), iData.data.Length);

            return arr;
        }

        private static imageData fromBytes(byte[] arr)
        {
            imageBaseData str = new imageBaseData();
            

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (imageBaseData)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            imageData iData = new imageData();
            iData.sliceNumber = str.sliceNumber;
            iData.last = str.last;

            iData.data = new byte[arr.Length - size];
            Buffer.BlockCopy(arr, size, iData.data, 0, iData.data.Length);

            return iData;
        }

        private static void SendDesktopImage()
        {
            //sends the screenshot to the server

            while (StaticVariable.isConnectedControled)
            {
                ScreenCapture sc = new ScreenCapture();
                Image img = sc.CaptureScreen();
                BinaryFormatter binFormatter = new BinaryFormatter();
               // mainStream = client.GetStream();

                //BinaryWriter writer = new BinaryWriter(mainStream);

                // save the image to a MemoryStream
                MemoryStream ms = new MemoryStream();
                //img.h;
                Bitmap bitmap = new Bitmap(img, new Size(320, 480));
                Image img2 = (Image)bitmap;
                img.Save(ms, ImageFormat.Jpeg);


                // get the image buffer
                byte[] buffer = new byte[ms.Length];
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(buffer, 0, buffer.Length);

                try
                {
                    //sendData(Protocol.serverGetImage);
                    //PartnerScreen.logWriteLine("(SendDesktopImage) send:" + Protocol.serverGetImage);
                    

                    // write the size of the image buffer
                    //int len = buffer.Length;
                    //sendData(len.ToString("000000"));
                    //PartnerScreen.logWriteLine("(SendDesktopImage) size of image:" + len.ToString("000000"));

                    Thread.Sleep(50);

                    lock (sendLock)
                    {
                        //writer.Write((Int32)2);//2 is video

                        for(int i=0;true; i++)
                        {
                            imageData slice;
                            

                           
                            if ((buffer.Length - i * UDP_BUFFER_SIZE) > UDP_BUFFER_SIZE)
                            {
                                slice.data = new byte[UDP_BUFFER_SIZE];
                                Buffer.BlockCopy(buffer, i * UDP_BUFFER_SIZE, slice.data, 0, UDP_BUFFER_SIZE);
                                slice.sliceNumber = i;
                                slice.last = false;

                                byte[] toSend = getBytes(slice);
                                udpClient.Send(toSend, toSend.Length);
                            }
                            else
                            {
                                slice.data = new byte[buffer.Length - i * UDP_BUFFER_SIZE];
                                Buffer.BlockCopy(buffer, i * UDP_BUFFER_SIZE, slice.data, 0, buffer.Length - i * UDP_BUFFER_SIZE);
                                slice.sliceNumber = i;
                                slice.last = true;

                                byte[] toSend = getBytes(slice);
                                udpClient.Send(toSend, toSend.Length);
                                break;
                            }
                            

                            //buffer.CopyTo(slice.data, i * 60000);

                            //udpClient.Send(buffer, buffer.Length);
                        }


                        

                        //PartnerScreen.logWriteLine("(SendDesktopImage) before send image");
                        // write the actual buffer
                        //writer.Write(buffer);
                        //writer.Flush();
                        //PartnerScreen.logWriteLine("(SendDesktopImage) sent:" + buffer.Length.ToString()+ " bytes");

                        //binFormatter.Serialize(mainStream, img);
                        //}
                    }
                }
                catch(Exception ex)
                {
                    PartnerScreen.logWriteLine("problem in sending image:");
                    PartnerScreen.logWriteLine(ex.Message);
                }
            }
        }

        private static void sendKeys_and_mouse()
        {
            string mouseXY;
            string mouseKeys;
            while (true)
            {
                Thread.Sleep(100);

                if (StaticVariable.keys.Count > 0 && StaticVariable.isConnectedControling)
                {
                    string key = StaticVariable.keys.Dequeue();
                    sendData(Protocol.keyToServer + key.Length.ToString("00") + key);
                }
                else if(!StaticVariable.isConnectedControling)
                {
                    StaticVariable.keys.Clear();
                }

                if (StaticVariable.isConnectedControling)
                {
                    mouseXY = StaticVariable.mouseX.ToString("0000000000") + StaticVariable.mouseY.ToString("0000000000");

                    sendData(Protocol.cursorPosToServer + mouseXY);
                }

                if (StaticVariable.mouseKeys.Count > 0 && StaticVariable.isConnectedControling)
                {
                    mouseKeys = StaticVariable.mouseKeys.Dequeue();
                    sendData(Protocol.cursorKeyToServer + mouseKeys.Length.ToString("00") + mouseKeys);
                }
                else if (!StaticVariable.isConnectedControling)
                {
                    StaticVariable.mouseKeys.Clear();
                    //Thread.Sleep(0);
                }
            }
        }

        //private static void sendCursPos()
        //{
        //    string mouseXY;

        //    while (true)
        //    {
        //        if (StaticVariable.isConnectedControling)
        //        {
        //            mouseXY = StaticVariable.mouseX.ToString("0000000000") + StaticVariable.mouseY.ToString("0000000000");

        //            sendData(Protocol.cursorPosToServer + mouseXY);
        //            Thread.Sleep(50);
        //        }
        //    }
        //}

        //private static void sendCurskeys()
        //{
        //    string mouseKeys;

        //    while (true)
        //    {
        //        if (StaticVariable.mouseKeys.Count > 0 && StaticVariable.isConnectedControling)
        //        {
        //            mouseKeys = StaticVariable.mouseKeys.Dequeue();
        //            sendData(Protocol.cursorKeyToServer + mouseKeys.Length.ToString("00") + mouseKeys);
        //            Thread.Sleep(50);
        //        }
        //        else if (!StaticVariable.isConnectedControling)
        //        {
        //            StaticVariable.mouseKeys.Clear();
        //            //Thread.Sleep(0);
        //        }
        //    }
        //}

        private static void GetImage()
        {
            while (StaticVariable.isConnectedControling)
            {
                try
                {
                    IPEndPoint newEndPoint = new IPEndPoint(0, 0);
                    byte[] imgBytes = new byte[0];
                    byte[] tempImgBytes;

                    int expectedSlice = 0;
                    
                    while (true)
                    {

                        byte[] slice = udpClient.Receive(ref newEndPoint);
                        imageData data = fromBytes(slice);
                        if(data.sliceNumber==expectedSlice)
                        {
                            int currBufferLen = imgBytes.Length;
                            if (currBufferLen > 0)
                            {
                                tempImgBytes = new byte[currBufferLen];
                                Buffer.BlockCopy(imgBytes, 0, tempImgBytes, 0, currBufferLen);
                                
                                imgBytes = new byte[currBufferLen + data.data.Length];
                                Buffer.BlockCopy(tempImgBytes, 0, imgBytes, 0, currBufferLen);
                            }
                            else
                            {
                                imgBytes = new byte[data.data.Length];
                            }
                            
                            
                            Buffer.BlockCopy(data.data, 0, imgBytes, currBufferLen, data.data.Length);

                            expectedSlice++;
                            if(data.last)
                            {
                                break;
                            }
                        }
                        else
                        {
                            PartnerScreen.logWriteLine("Lost slice, expected:" + expectedSlice.ToString() + ", recived:" + data.sliceNumber.ToString());
                            expectedSlice = 0;
                            imgBytes = new byte[0];
                            continue;
                        }
                    }

                    MemoryStream ms = new MemoryStream(imgBytes);
                    Image img = Image.FromStream(ms);

                    PartnerScreen.logWriteLine("Enqueue image");
                    lock (StaticVariable.imgs)
                    {
                        StaticVariable.imgs.Enqueue(img);
                    }
                }
                catch (Exception ex)
                {
                    PartnerScreen.logWriteLine("(GetImage) exeption: " + ex.Message);
                }
            }

        }


        private static void HandleConnection()
        {
            //string sData = "";
            client.NoDelay = true;

            StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);

            Thread listen = new Thread(new ParameterizedThreadStart(clientListener));
            listen.Start(sReader);

            //while (true)
            //{


                // reads from stream
                //sData = sReader.ReadLine();

                // shows content on the console.
                //Console.WriteLine("Client " + id.ToString() + " said " + sData);

                // to write something back.
                // sWriter.WriteLine("Meaningfull things here");
                // sWriter.Flush();
            //}
        }

        private static void clientListener(object obj)
        {
            int strLen = 0;
            StreamReader sReader = (StreamReader)obj;
            char[] buff = new char[1024];
            int num = 0;

            try
            {
                while (true)//while client running
                {
                    sReader.ReadBlock(buff, 0, 3);//read 3 characters from server
                    string code = new string(buff).Substring(0, 3);

                    PartnerScreen.logWriteLine("read code:" + code);

                    switch (code)
                    {
                        case Protocol.ID:
                            PartnerScreen.logWriteLine("case Protocol.ID:");
                            sReader.ReadBlock(buff, 0, 3);//read 3 characters from server
                            string id = new string(buff).Substring(0, 3);
                            StaticVariable.ID = int.Parse(id);
                            PartnerScreen.logWriteLine("id is "+ id);
                            break;

                        case Protocol.connectionRequest:
                            PartnerScreen.logWriteLine("case Protocol.connectionRequest:");
                            sReader.ReadBlock(buff, 0, 2);//read 2 characters from server
                            num = int.Parse(new string(buff).Substring(0,2));

                            sReader.ReadBlock(buff, 0, num);
                            string password = new string(buff).Substring(0, num);
                            PartnerScreen.logWriteLine("password: "+ password);
                            if (password == StaticVariable.Password)
                            {
                                sendData(Protocol.connectionAcceptable);
                                PartnerScreen.logWriteLine("Connection Acceptable");
                            }
                            else
                            {
                                sendData(Protocol.wrongPassword);
                                PartnerScreen.logWriteLine("Wrong Password");
                            }
                            break;

                        case Protocol.connectionFailedNoID:
                            PartnerScreen.logWriteLine("case Protocol.connectionFailedNoID:");
                            StaticVariable.messageFromServer = "no such id";
                            break;

                        case Protocol.connectionSecceeded_Controling:
                            PartnerScreen.logWriteLine("case Protocol.connectionSecceeded_Controling:");

                            StaticVariable.isConnectedControling = true;
                            StaticVariable.messageFromServer = "Succesful Connection,now controling";

                            PartnerScreen.logWriteLine("start thread: sendKeys_and_mouse");
                            Thread sendKeysAndMouse = new Thread(sendKeys_and_mouse);
                            sendKeysAndMouse.Start();
                            Thread getImage = new Thread(GetImage);
                            getImage.Start();
                            //Thread send_cursorPos = new Thread(sendCursPos);
                            //send_cursorPos.Start();
                            //Thread send_cursorkeys = new Thread(sendCurskeys);
                            //send_cursorkeys.Start();

                            break;

                        case Protocol.connectionSecceeded_Controled:
                            PartnerScreen.logWriteLine("case Protocol.connectionSecceeded_Controled:");

                            StaticVariable.isConnectedControled = true;
                            StaticVariable.messageFromServer = "Succesful Connection,now being controled";
                            PartnerScreen.logWriteLine("start thread: SendDesktopImage");
                            Thread sendImage = new Thread(SendDesktopImage);
                            sendImage.Start();
                            break;

                        case Protocol.connectionFailedPassword:
                            PartnerScreen.logWriteLine("case Protocol.connectionFailedPassword:");
                            StaticVariable.messageFromServer = "wrong password";
                            break;

                        case Protocol.connectionFailed:
                            PartnerScreen.logWriteLine("case Protocol.connectionFailed:");
                            StaticVariable.messageFromServer = "failed for some reason";
                            break;

                        case Protocol.getImage:
                            PartnerScreen.logWriteLine("case Protocol.getImage:");

                            mainStream = client.GetStream();

                            BinaryReader reader = new BinaryReader(mainStream);

                            // read how big the image buffer is
                            char[] imgSizeBuffer = new char[6];
                            sReader.Read(imgSizeBuffer, 0, 6);
                            string intiger = new string(imgSizeBuffer);
                            PartnerScreen.logWriteLine("read image with size: " + intiger);
                            int imgSizeBytes = int.Parse(intiger);
                            

                            // read the image buffer into a MemoryStream
                            MemoryStream ms = new MemoryStream(reader.ReadBytes(imgSizeBytes));

                            Image img = Image.FromStream(ms);

                            PartnerScreen.logWriteLine("Enqueue image");
                            lock(StaticVariable.imgs)
                            {
                                StaticVariable.imgs.Enqueue(img);
                            }
                            
                            break;

                        case Protocol.keyToClient:
                            PartnerScreen.logWriteLine("case Protocol.keyToClient:");
                            sReader.ReadBlock(buff, 0, 2);
                            strLen = int.Parse(new string(buff).Substring(0, 2));
                            sReader.ReadBlock(buff, 0, strLen);
                            string key = new string(buff).Substring(0, strLen);
                            PartnerScreen.logWriteLine("get key: \""+ key+ "\" and send it");
                            SendKeys.SendWait(key);
                            break;

                        case Protocol.cursorPosToClient:
                            PartnerScreen.logWriteLine("case Protocol.cursorPosToClient:");
                            sReader.ReadBlock(buff, 0, 20);
                            int cursorX = int.Parse(new string(buff).Substring(0, 10));
                            PartnerScreen.logWriteLine("cursorX: " + cursorX.ToString());
                            //sReader.ReadBlock(buff, 0, 10);
                            int cursorY = int.Parse(new string(buff).Substring(10, 10));
                            PartnerScreen.logWriteLine("cursorY: " + cursorY.ToString());

                            //Thread.Sleep(20);
                            WinAPI.MouseMove(cursorX, cursorY);
                            break;

                        case Protocol.cursorKeyToClient:
                            PartnerScreen.logWriteLine("case Protocol.cursorKeyToClient:");
                            sReader.ReadBlock(buff, 0, 2);
                            strLen = int.Parse(new string(buff).Substring(0, 2));
                            sReader.ReadBlock(buff, 0, strLen);
                            string cursorKey = new string(buff).Substring(0, strLen);

                            PartnerScreen.logWriteLine("get cursor key: \"" + cursorKey + "\" and send it");

                            WinAPI.MouseClick(cursorKey);
                            break;

                        default:
                            PartnerScreen.logWriteLine("code not valid"); 
                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                PartnerScreen.logWriteLine("(clientListener) exeption: " + ex.Message);
            }
        }

        static public void sendData(string dataString,TcpClient _client = null)
        {
            if(_client == null)
            {
                _client = client;
            }
            lock(sendLock)
            {
                mainStream = _client.GetStream();
                BinaryWriter writer = new BinaryWriter(mainStream);

                StreamWriter sWriter = new StreamWriter(_client.GetStream(), Encoding.ASCII);
                PartnerScreen.logWriteLine("(sendData)*** before send: " + dataString + dataString.Length);
                sWriter.Write(dataString);
                sWriter.Flush();
                PartnerScreen.logWriteLine("(sendData)*** after send: " + dataString + dataString.Length);


            }
        }

        static void testFunc()
        {
            FileStream fs = new FileStream("DataFile.dat", FileMode.Create);
            ScreenCapture sc = new ScreenCapture();
            Image img = sc.CaptureScreen();
            new testDisplayForm((Bitmap)img).Show();
            BinaryFormatter binFormatter = new BinaryFormatter();
            binFormatter.Serialize(fs, img);
            fs.Close();
            fs = new FileStream("DataFile.dat", FileMode.Open);
            Image img2 = (Image)binFormatter.Deserialize(fs);
            new testDisplayForm((Bitmap)img2).Show();
        }
        
    }
}
