﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using Afkvalley;

namespace TeamViewer_Client
{
    public partial class PartnerScreen : Form
    {
        private readonly Thread getImage;

        //private static NetworkStream mainStream;
        public PartnerScreen(TcpClient client)
        {
            getImage = new Thread(new ParameterizedThreadStart(ReceiveImage));
            getImage.Start(client);
            Thread Mouse_Move1 = new Thread(Mouse_Move);
            Mouse_Move1.Start();

            InitializeComponent();

        }
        //    public PartnerScreen(TcpClient client, string method)
        //    {
        //        getImage = new Thread(new ParameterizedThreadStart(ReceiveImage));

        //        switch (method):
        //{
        //case "controling":
        //            {
        //                getImage.Start(client);
        //                sendCursorPos = new Thread(new ParameterizedThreadStart(SendCursorPos);
        //                sendCursorPos.Start(Cursor);
        //                break;
        //            }
        //case "viewing":
        //            {
        //                getImage.Start(client);
        //                break;
        //            }


        //            InitializeComponent();
        //        }
        private void Mouse_Move()
        {
            while (true)
            {
                StaticVariable.mouseX = Cursor.Position.X;
                StaticVariable.mouseY = Cursor.Position.Y;
            }
        }

        private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            StaticVariable.mouseX = Cursor.Position.X;
            StaticVariable.mouseY = Cursor.Position.Y;

        }
        private void pictureBox1_MouseHover(object sender, System.EventArgs e)
        {
            StaticVariable.mouseX = Cursor.Position.X;
            StaticVariable.mouseY = Cursor.Position.Y;
        }

        
        
        private void ReceiveImage(object obj)
        {

            TcpClient client = (TcpClient)obj;
            //BinaryFormatter binFormatter = new BinaryFormatter();

            while (client.Connected)
            {
                try
                {
                    //mainStream = client.GetStream();
                    //byte[] img = new byte[1024];
                    //mainStream.Read(img, 0, 1024);

                    //MessageBox.Show(System.Text.Encoding.Default.GetString(img));

                    //Image image;
                    //using (MemoryStream m = new MemoryStream(img))
                    //{
                    //    image = Image.FromStream(m);
                    //}

                    //pictureBox1.Image = image;
                    //mainStream = client.GetStream();

                    //BinaryReader reader = new BinaryReader(mainStream);

                    // read how big the image buffer is
                    //int ctBytes = reader.ReadInt32();

                    // read the image buffer into a MemoryStream
                    //MemoryStream ms = new MemoryStream(reader.ReadBytes(ctBytes));

                    //Image img = Image.FromStream(ms);
                    Thread.Sleep(10);
                    if (StaticVariable.imgs.Count() > 0)
                    {
                        lock (StaticVariable.imgs)
                        {
                            pictureBox1.Image = StaticVariable.imgs.Dequeue();
                        }
                           
                        PartnerScreen.logWriteLine("(PartnerScreen) Dequeued image");
                        PartnerScreen.logWriteLine("(PartnerScreen) StaticVariable.imgs.Count():" + StaticVariable.imgs.Count().ToString());
                    }
                    else
                    {
                        Thread.Sleep(100);
                        PartnerScreen.logWriteLine("(PartnerScreen) StaticVariable.imgs.Count():" + StaticVariable.imgs.Count().ToString());
                    }


                    //pictureBox1.Image = (Image)binFormatter.Deserialize(mainStream);

                }
                catch (Exception ex)
                {
                    PartnerScreen.logWriteLine("(PartnerScreen) exeption:"+ ex.Message);
                    MessageBox.Show(ex.Message);
                }
            }
        }

        static public Image ByteToImage(byte[] img)
        {
            ImageConverter converter = new ImageConverter();
            return (Image)converter.ConvertFrom(img);
        }

        private void PartnerScreen_KeyPress(object sender, KeyPressEventArgs e)
        {
            StaticVariable.keys.Enqueue(e.KeyChar.ToString());
        }

        private void pictureBox1_MouseDown_1(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    StaticVariable.mouseKeys.Enqueue("left");
                    break;
                case MouseButtons.Right:
                    StaticVariable.mouseKeys.Enqueue("right");
                    break;
                case MouseButtons.Middle:
                    StaticVariable.mouseKeys.Enqueue("middle");
                    break;
                case MouseButtons.None:
                default:
                    break;

            }
        }

        static public void logWriteLine(string line)
        {
            string _line = DateTime.Now.ToLongTimeString() + ", " + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "  --  " + line + Environment.NewLine;
            _logWrite(_line);
        }

        static public void _logWrite(string text)
        {

            StaticVariable.logFile.Write(Encoding.ASCII.GetBytes(text), 0, text.Length);
            StaticVariable.logFile.Flush();
        }

        private void PartnerScreen_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
