﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Threading;
using TeamViewer_Server;
using System.IO;

namespace TeamViewer_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            File.Delete("clientLog.txt");
            StaticVariable.logFile = File.OpenWrite("clientLog.txt");
            Thread PartnerStatus = new Thread(updatePartnerStatus);
            PartnerStatus.Start();
            Thread Password = new Thread(updatePassword);
            Password.Start();
            Thread Status = new Thread(updateStatus);
            Status.Start();
            //new FileTransfer().Show();
            InitializeComponent();

        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
           
            Networking.Connection(int.Parse(txtPort.Text), txtIP.Text);
        }
        private void updateStatus()
        {
            while(true)
            {
                Thread.Sleep(100);

                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (StaticVariable.ID != 0)
                    {
                        status.Content ="ID:"+ StaticVariable.ID.ToString();
                        Password.Content = "Password:"+StaticVariable.Password;
                    }

                }));
            }
        }
        private void updatePartnerStatus()
        {
            while (true)
            {
                Thread.Sleep(100);

                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (StaticVariable.ID != 0)
                    {
                        partnerConnectionStatus.Content = StaticVariable.messageFromServer;
                    }

                }));
            }
        }
        private void updatePassword()
        {
            while (true)
            {
                Thread.Sleep(500);
                this.Dispatcher.BeginInvoke(new Action(() =>
                {

                    StaticVariable.Password = txtBoxPassword.Text;
                }));
                
            }
        }

        private void btnConToPartner_Click(object sender, RoutedEventArgs e)
        {            
            if(!Networking.client.Connected)
            {
                MessageBox.Show("connect to server first");
            }
            else
            {
                try
                {
                    
                    if (int.Parse(partnerIdTxt.Text) >= 100)
                        {
                            Networking.sendData(Protocol.connectToOtherClient + partnerIdTxt.Text + txtBoxPassword.Text.Length.ToString("00") + partnerPassTxt.Text);
                        }
                    else
                        {
                            MessageBox.Show("ID invalid");
                        }
                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            //Thread partscrn = new Thread(PartnerScreenOn);
            //partscrn.Start();
            //Thread.Sleep(1000);
            //while (!StaticVariable.partnerScreenOn && StaticVariable.isConnectedControling)
            //{
                Thread.Sleep(1000);
                new PartnerScreen(Networking.client).Show();
                StaticVariable.partnerScreenOn = true;
            //    Thread.Sleep(1000);
            //}

        }

        private void PartnerScreenOn()
        {
            while(!StaticVariable.partnerScreenOn)
            {
                if (!StaticVariable.partnerScreenOn && StaticVariable.isConnectedControling)
                {
                    new PartnerScreen(Networking.client).Show();
                    StaticVariable.partnerScreenOn = true;
                }
            }
        }

        //private void OnKeyDownHandler(object sender, KeyEventArgs e)
        //{
        //    if (StaticVariable.isConnectedControling)
        //    {
        //        StaticVariable.keys.Enqueue(e.Key.ToString());
        //    }
        //}


    }
}
