﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeamViewer_Client
{
    public partial class testDisplayForm : Form
    {
        Bitmap BM;

        public testDisplayForm(Bitmap bm)
        {
            BM = bm;
            InitializeComponent();
        }

        private void testDisplayForm_Load(object sender, EventArgs e)
        {
            Paint += new PaintEventHandler(Form1_Paint); //Link the Paint event to Form1_Paint; you can do this within the designer too!
        }

        private void print(Bitmap BM, PaintEventArgs e)
        {
            Graphics graphicsObj = e.Graphics; //Get graphics from the event
            graphicsObj.DrawImage(BM, 60, 10); // or "e.Graphics.DrawImage(bitmap, 60, 10);"
            graphicsObj.Dispose();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
            print(BM, e);
        }
    }
}
