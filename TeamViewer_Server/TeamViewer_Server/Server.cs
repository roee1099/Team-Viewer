﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;


namespace TeamViewer_Server
{
    class Server
    {

        private bool _isRunning;
        private readonly int _port;
        private TcpListener _server;
        private TcpClient _client;
        private UdpClient _UDPlistener;

        public class UDPConnectionParam
        {
            public UdpClient client;
            public IPEndPoint endPoint;

            public UDPConnectionParam(UdpClient _client, IPEndPoint _endPoint)
            {
                client = _client;
                endPoint = _endPoint;
            }
        }
        
        //private NetworkStream _mainstream;
        private readonly Thread Listening;
        //private FileStream fstream;

        private Dictionary<int, TcpClient> ids = new Dictionary<int, TcpClient>();
        private Dictionary<TcpClient, int> idsByClient = new Dictionary<TcpClient, int>();

        private Dictionary<TcpClient, TcpClient> controllingToControlled = new Dictionary<TcpClient, TcpClient>();
        private Dictionary<TcpClient, TcpClient> controlledToControlling = new Dictionary<TcpClient, TcpClient>();

        private Dictionary<UDPConnectionParam, TcpClient> UDPtoTCP = new Dictionary<UDPConnectionParam, TcpClient>();
        private Dictionary<TcpClient, UDPConnectionParam> TCPtoUDP = new Dictionary<TcpClient, UDPConnectionParam>();

        private Dictionary<TcpClient, FileStream> logFiles = new Dictionary<TcpClient, FileStream>();

        public Server(int port)
        {
            _port = port;
            _client = new TcpClient();
            _server = new TcpListener(IPAddress.Any, _port);
            Console.WriteLine("listening on port:" + _port.ToString());
            _isRunning = true;
            
            Listening = new Thread(startListening);
            Listening.Start();

            _UDPlistener = new UdpClient();
            _UDPlistener.Client.Bind(new IPEndPoint(IPAddress.Any, _port + 1));
                
        }

        private void startListening()
        {
            _server.Start();
            while (_isRunning)
            {
                _client = _server.AcceptTcpClient();
                Console.WriteLine("client accepted");

                IPEndPoint _ipEnd = new IPEndPoint(0,0);
                _UDPlistener.Receive(ref _ipEnd);

                UdpClient _vidClient = new UdpClient();
                //_vidClient.Connect(_ipEnd);
                _vidClient.Send(new byte[] {1}, 1, _ipEnd);
                byte[] recivedData = _vidClient.Receive(ref _ipEnd);
                _vidClient.Connect(_ipEnd);
                UDPConnectionParam param = new UDPConnectionParam(_vidClient, _ipEnd);

                UDPtoTCP.Add(param, _client);
                TCPtoUDP.Add(_client, param);

                Thread t = new Thread(new ParameterizedThreadStart(HandleClient));
                t.Start(_client);
            }
        }

        private void HandleClient(object obj)
        {
            TcpClient client = (TcpClient)obj;

            StreamWriter sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII);
            StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);

            //Boolean bClientConnected = true;
            //String sData = null;

            try
            {
                int id = Logic.generateId();
                ids.Add(id, client);
                idsByClient.Add(client, id);
                Console.WriteLine("new client id is " + id.ToString());

                string fileName = "Log" + id.ToString() + ".txt";
                File.Delete(fileName);
                logFiles.Add(client, File.OpenWrite(fileName));

                sWriter.Write(Protocol.ID + id.ToString());
                sWriter.Flush();

                logWriteLine("(HandleClient) id sent to client", client);

                Thread listen = new Thread(new ParameterizedThreadStart(serverListener));
                listen.Start(client);

                logWriteLine("(HandleClient) serverListener thread started", client);

                //Thread imageTrans = new Thread(new ParameterizedThreadStart(imageTransfer));
                //imageTrans.Start(client);

                //while (bClientConnected)
                //{
                // reads from stream
                //sData = sReader.ReadLine();

                // shows content on the console.
                //Console.WriteLine("Client " + id.ToString() + " said " + sData);

                // to write something back.
                // sWriter.WriteLine("Meaningfull things here");
                // sWriter.Flush();
                //}
            }
            catch (Exception ex)
            {
                if (ex.Message == "Unable to read data from the transport connection: An existing connection was forcibly closed by the remote host.")
                {
                    closeConnection(client);
                    //bClientConnected = false;
                }
            }
        }

        //private void dataSort(object obj)
        //{
        //    try
        //    {
        //        NetworkStream mainStream1;
        //        TcpClient client = (TcpClient)obj;

        //        mainStream1 = client.GetStream();

        //        BinaryReader reader = new BinaryReader(mainStream1);

        //        while (true)
        //        {
        //            int dataType = reader.ReadInt32();
        //            switch (dataType)
        //            {
        //                case 1://command
        //                    serverListener(client);
        //                    break;
        //                case 2://video
        //                    imageTransfer(client);
        //                    break;
        //                default:
        //                    Console.WriteLine("unknown dataType");
        //                    break;
        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //}

        private void imageTransfer(object obj)
        {
            while (true)
            {
                TcpClient client = (TcpClient)obj;

                try
                {
                    UDPConnectionParam vidClient = TCPtoUDP[client];
                    byte[] img = vidClient.client.Receive(ref vidClient.endPoint);
                    if (controlledToControlling.ContainsKey(client))
                    {
                        TcpClient otherCliebt = controlledToControlling[client];
                        UDPConnectionParam param = TCPtoUDP[otherCliebt];
                        param.client.Send(img, img.Length);

                        StreamWriter sWriter = new StreamWriter(controlledToControlling[client].GetStream(), Encoding.ASCII);
                        StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);
                    }
                }
                catch (Exception ex)
                {
                    logWriteLine("(imageTransfer) Excption: " + ex.Message, client);
                }
            }
            



            /*
            //while (true)
            //{
            

            NetworkStream mainStream1;
            NetworkStream mainStream2;
            try
            {
                mainStream1 = client.GetStream();
                if(controlledToControlling.ContainsKey(client))
                {
                    mainStream2 = controlledToControlling[client].GetStream();
                    StreamWriter sWriter = new StreamWriter(controlledToControlling[client].GetStream(), Encoding.ASCII);
                    StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);

                    BinaryFormatter binFormatter = new BinaryFormatter();
                    //binFormatter.Serialize(mainStream2, binFormatter.Deserialize(mainStream1));
                    BinaryReader reader = new BinaryReader(mainStream1);

                    // read how big the image buffer is
                    char[] imgSizeBuffer = new char[6];
                    sReader.Read(imgSizeBuffer, 0, 6);
                    string intiger = new string(imgSizeBuffer);
                    logWriteLine("(imageTransfer) read size of image. size is:" + intiger, client);
                    int imgSizeBytes = int.Parse(intiger);
                    

                    // read the image buffer into a MemoryStream
                    MemoryStream ms = new MemoryStream(reader.ReadBytes(imgSizeBytes));
                    logWriteLine("(imageTransfer) read " + ms.Length.ToString() + " bytes form client", client);

                    BinaryWriter writer = new BinaryWriter(mainStream2);
                    

                    byte[] buffer = new byte[ms.Length];
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.Read(buffer, 0, buffer.Length);
                    logWriteLine("(imageTransfer) created buffer with size:" + buffer.Length.ToString(), client);
                    
                    sendData(Protocol.getImage, sWriter);
                    logWriteLine("(imageTransfer) send to other:" + Protocol.getImage, client);
                    logWriteLine("(" + idsByClient[client].ToString() + ") send:" + Protocol.getImage, controlledToControlling[client]);

                    // write the size of the image buffer
                    int len = buffer.Length;
                    sendData(len.ToString("000000"), sWriter);
                    
                    logWriteLine("(imageTransfer) send image size to other:" + len.ToString(), client);
                    logWriteLine("(" + idsByClient[client].ToString() + ") send image size:" + len.ToString(), controlledToControlling[client]);

                    // write the actual buffer
                    writer.Write(buffer);
                    writer.Flush();
                    logWriteLine("(imageTransfer) send image to other", client);
                    logWriteLine("(" + idsByClient[client].ToString() + ") send image", controlledToControlling[client]);

                    // get the image from the MemoryStream
                    //Image img = Image.FromStream(ms);

                    //binFormatter.Serialize(mainStream2, img);

                    //new TeamViewer_Client.testDisplayForm((Bitmap)img).Show();
                    // = (Image)binFormatter.Deserialize(mainStream);

                    //byte[] buff = new byte[1024];
                    //int num = mainStream1.Read(buff, 0, 1024);
                    //mainStream2.Write(buff, 0, num);

                }
                else
                {
                    throw new Exception("controlledToControlling do not Contains Key");
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "Unable to read data from the transport connection: An existing connection was forcibly closed by the remote host.")
                {
                    closeConnection(client);
                }
                else
                {
                    logWriteLine("(imageTransfer) Excption: " + ex.Message, client);
                }
            }








                //binFormatter.Serialize(mainStream, grabDesktop());
                //BinaryFormatter binFormatter = new BinaryFormatter();
                //binFormatter.Deserialize(mainStream);
            //}
            */
        }       

        private void serverListener(object obj)
        {
            //StreamReader sReader = (StreamReader)obj;
            TcpClient client = (TcpClient)obj;
            StreamWriter sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII);
            StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);
            int strLen = 0;



            char[] buff = new char[1024];

            try
            {
                int num = 0;
                StreamWriter other;
                while (true)//while server running
                {
                    logWriteLine("(serverListener) back to code reading", client);
                    sReader.ReadBlock(buff, 0, 3);//read 3 characters from server
                    string code = new string(buff).Substring(0, 3);

                    logWriteLine("(serverListener) read code:" + code, client);
                    
                    switch (code)
                    {
                        case Protocol.textMessageToServer:

                            try
                            {
                                logWriteLine("(serverListener) case: + Protocol.textMessageToServer", client);
                                sReader.ReadBlock(buff, 0, 2);
                                num = int.Parse(new string(buff).Substring(0, 2));
                                logWriteLine("(serverListener) read length:" + num.ToString(), client);

                                sReader.ReadBlock(buff, 0, num);
                                string message = new string(buff).Substring(0, num);
                                logWriteLine("(serverListener) read message:" + message, client);

                                Console.WriteLine(message);

                            }
                            catch (Exception)
                            {
                                
                                throw;
                            };

                            break;

                        case Protocol.connectToOtherClient:

                            try
                            {
                                logWriteLine("(serverListener) case: Protocol.connectToOtherClient", client);

                                sReader.ReadBlock(buff, 0, 3);
                                int targetID = int.Parse(new string(buff).Substring(0, 3));
                                logWriteLine("(serverListener) read target id:" + targetID.ToString(), client);

                                sReader.ReadBlock(buff, 0, 2);
                                num = int.Parse(new string(buff).Substring(0, 2));
                                logWriteLine("(serverListener) read length:" + num.ToString(), client);

                                sReader.ReadBlock(buff, 0, num);
                                string password = new string(buff).Substring(0, num);
                                logWriteLine("(serverListener) read password:" + password, client);

                                if (targetID != idsByClient[client])
                                {
                                    logWriteLine("(serverListener) id is not sender's id", client);
                                    if (ids.ContainsKey(targetID))
                                    {
                                        logWriteLine("(serverListener) id is in list of ids", client);
                                        TcpClient targetClient = ids[targetID];
                                        StreamWriter targetWriter = new StreamWriter(targetClient.GetStream(), Encoding.ASCII);

                                        controllingToControlled.Add(client, targetClient);//temporerly insert them until check password
                                        controlledToControlling.Add(targetClient, client);
                                        logWriteLine("(serverListener) temporerly insert clients to controllingToControlled and controlledToControlling until check password", client);

                                        string data = Protocol.connectionRequest + password.Length.ToString("00") + password;
                                        sendData(data, targetWriter);
                                        logWriteLine("(serverListener) sending connection request to:" + targetID.ToString()+". data:" + data, client);
                                        logWriteLine("(" + targetID.ToString() + ") is tryning to connect to you", targetClient);

                                        Console.WriteLine("User: " + idsByClient[client] + " is trying to connect to: " + targetID);
                                        
                                    }
                                    else
                                    {
                                        logWriteLine("(serverListener) id is not in list of ids", client);
                                        sendData(Protocol.connectionFailedNoID, sWriter);
                                    }
                                }
                                else
                                {
                                    logWriteLine("(serverListener) id is sender's id", client);
                                    sendData(Protocol.connectionFailed, sWriter);
                                }
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                            break;

                        case Protocol.connectionAcceptable:
                            try
                            {
                                logWriteLine("(serverListener) case: Protocol.connectionAcceptable", client);
                                logWriteLine("(" + idsByClient[client].ToString() + ") can connect to you", controlledToControlling[client]);
                                other = new StreamWriter(controlledToControlling[client].GetStream(), Encoding.ASCII);
                                sendData(Protocol.connectionSecceeded_Controled, sWriter);
                                sendData(Protocol.connectionSecceeded_Controling, other);

                                Thread imgTransfer = new Thread(new ParameterizedThreadStart(imageTransfer));
                                imgTransfer.Start(client);
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                            break;

                        case Protocol.wrongPassword:
                            try
                            {
                                logWriteLine("(serverListener) case: Protocol.wrongPassword", client);
                                logWriteLine("(" + idsByClient[client].ToString() + ") can't connect to you - wrong password", controlledToControlling[client]);
                                other = new StreamWriter(controlledToControlling[client].GetStream(), Encoding.ASCII);
                                sendData(Protocol.connectionFailedPassword, other);
                                TcpClient temp = controlledToControlling[client];
                                controlledToControlling.Remove(client);
                                controllingToControlled.Remove(temp);
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                            break;

                        case Protocol.keyToServer:
                            try
                            {
                                logWriteLine("(serverListener) case: Protocol.keyToServer", client);
                                sReader.ReadBlock(buff, 0, 2);
                                strLen = int.Parse(new string(buff).Substring(0, 2));
                                sReader.ReadBlock(buff, 0, strLen);
                                string key = new string(buff).Substring(0, strLen);

                                other = new StreamWriter(controllingToControlled[client].GetStream(), Encoding.ASCII);
                                sendData(Protocol.keyToClient + key.Length.ToString("00") + key, other);
                                logWriteLine("(" + idsByClient[client].ToString() + ") sending key:" + key, controllingToControlled[client]);
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                            break;

                        case Protocol.cursorPosToServer:
                            try
                            {
                                logWriteLine("(serverListener) case: Protocol.cursorPosToServer", client);
                                sReader.ReadBlock(buff, 0, 20);

                                if (controllingToControlled.ContainsKey(client))
                                {
                                    other = new StreamWriter(controllingToControlled[client].GetStream(), Encoding.ASCII);
                                    string cursorPos = new string(buff).Substring(0,20);
                                    sendData(Protocol.cursorPosToClient + cursorPos, other);
                                    logWriteLine("(" + idsByClient[client].ToString() + ") sending cursor position:" + cursorPos, controllingToControlled[client]);
                                }
                                else
                                {
                                    throw new Exception("controllingToControlled do not Contains Key");
                                }
                            }
                            catch (Exception)
                            {

                                throw;
                            }

                            break;

                        case Protocol.cursorKeyToServer:
                            try
                            {
                                logWriteLine("(serverListener) case: Protocol.cursorKeyToServer", client);
                                sReader.ReadBlock(buff, 0, 2);
                                strLen = int.Parse(new string(buff).Substring(0, 2));
                                sReader.ReadBlock(buff, 0, strLen);
                                string cursorKey = new string(buff).Substring(0, strLen);

                                other = new StreamWriter(controllingToControlled[client].GetStream(), Encoding.ASCII);
                                sendData(Protocol.cursorKeyToClient + strLen.ToString("00") + cursorKey, other);
                                logWriteLine("(" + idsByClient[client].ToString() + ") sending cursor key:" + cursorKey, controllingToControlled[client]);
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                            break;

                        case Protocol.serverGetImage:
                            try
                            {
                                //logWriteLine("(serverListener) case: Protocol.serverGetImage", client);
                                
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                            break;

                        default:
                            logWriteLine("(serverListener) code:" + code + "not valid", client);
                            Console.WriteLine("code not valid");
                            break;

                    }
                }
            }
            catch(Exception ex)
            {
                logWriteLine("(serverListener) Excption: " + ex.Message, client);
                if (ex.Message == "Unable to read data from the transport connection: An existing connection was forcibly closed by the remote host.")
                {
                    closeConnection(client);
                }
            }
        }

        static public void sendData(string dataString, StreamWriter sWriter)
        {
            sWriter.Write(dataString);
            sWriter.Flush();
        }

        private void closeConnection(TcpClient client)
        {
            try
            {
                int id = idsByClient[client];
                idsByClient.Remove(client);
                ids.Remove(id);

                Logic.ids.Remove(id);

                if (controllingToControlled.ContainsKey(client))
                {
                    TcpClient c = controllingToControlled[client];
                    controllingToControlled.Remove(client);
                    controlledToControlling.Remove(c);
                }

                if (controlledToControlling.ContainsKey(client))
                {
                    TcpClient c = controlledToControlling[client];
                    controlledToControlling.Remove(client);
                    controllingToControlled.Remove(c);
                }

                if (TCPtoUDP.ContainsKey(client))
                {
                    UDPConnectionParam c = TCPtoUDP[client];
                    TCPtoUDP.Remove(client);
                    UDPtoTCP.Remove(c);
                    c.client.Close();
                }

                Console.WriteLine("client - " + id.ToString() + " disconnected");

                logWriteLine("client - " + id.ToString() + " disconnected", client);
                logFiles.Remove(client);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        private void logWriteLine(string line, TcpClient client)
        {
            string _line = DateTime.Now.ToLongTimeString()+ ", " + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "  --  " + line + Environment.NewLine;
            _logWrite(_line, client);
        }

        private void _logWrite(string text, TcpClient client)
        {
            
            if(logFiles.ContainsKey(client))
            {
                FileStream fstream = logFiles[client];
                fstream.Write(Encoding.ASCII.GetBytes(text), 0, text.Length);
                fstream.Flush();
            }
            else
            {
                throw new ArgumentException("Client is not in logFiles","client");
            }
        }
    }
}
