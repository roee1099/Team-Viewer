﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TeamViewer_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            const int port = 8888;
            Server serv = new Server(port);
            bool exit = false;

            while (exit == false)
            {
                string s = Console.ReadLine();
                if (s == "exit")
                {
                    exit = true;
                }
            }
        }
    }
}
