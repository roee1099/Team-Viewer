﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamViewer_Server
{
    static class Logic
    {
        static Random rnd = new Random();
        static public List<int> ids = new List<int>();
        static public int generateId()
        {
            int num = 0;
            do
            {
                num = rnd.Next(100, 1000);
            } while (ids.Contains(num));
            ids.Add(num);
            return num;
        }
    }
}
