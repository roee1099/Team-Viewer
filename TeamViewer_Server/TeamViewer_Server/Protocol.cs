﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamViewer_Server
{
    static class Protocol
    {
        //# is digit for length of next part of message

        //server -> client send ID - 100XXX(ID)
        public const string ID = "100";

        //server -> client connection request with password - 101##X(Password)
        public const string connectionRequest = "101";

        //                           000##X(message)
        public const string textMessageToServer = "000";

        //client -> server           200XXX(ID)##X(password)
        public const string connectToOtherClient = "200";

        //client -> server           201###XXX(protocol)X(rest of message)
        public const string sendToOtherClient = "201";

        //client -> server           202
        public const string connectionAcceptable = "202";

        //server -> client connection between client to other client Secceeded -Controling side - 300
        public const string connectionSecceeded_Controling = "300";

        //server -> client connection between client to other client Failed because no such ID - 301
        public const string connectionFailedNoID = "301";

        //server -> client connection between client to other client Failed because wrong password - 302
        public const string connectionFailedPassword = "302";

        //server -> client connection between client to other client Failed other reason - 303
        public const string connectionFailed = "303";

        //server -> client get image from server - 305
        public const string getImage = "305";

        //server -> client get key from server - 307
        public const string keyToClient = "307";

        //server -> client get mouse position from server - 308
        public const string cursorPosToClient = "308";

        //client -> server get mouse key from server - 309
        public const string cursorKeyToClient = "309";

        //server -> client connection between client to other client Secceeded - Controled side - 310
        public const string connectionSecceeded_Controled = "310";

        //client -> server connection between client to other client Failed because wrong password - 402
        public const string wrongPassword = "402";

        //server -> client get image from server - 305
        public const string serverGetImage = "405";

        //client -> server send key to server - 407
        public const string keyToServer = "407";

        //client -> server send mouse position - 408
        public const string cursorPosToServer = "408";

        //client -> server send mouse key - 409
        public const string cursorKeyToServer = "409";
    }
}
